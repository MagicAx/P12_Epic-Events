import datetime
from datetime import timedelta

from django.test import TestCase
from django.contrib.auth.models import Group

from constants import CREATED
from core.models import Employee
from api.models import Client, ClientAssignment, Contract, ContractNegotiationAssignment, ContractSignatureAssignment, \
    ContractPaymentAssignment, Event


class TestClient(TestCase):
    def setUp(self):
        Group.objects.get_or_create(id=2, name="Sales")
        self.sales_departement = Group.objects.get(id=2)
        Employee.objects.get_or_create(username="lprevosteau", first_name="Louis", last_name="Prevosteau",
                                                  email="louis.prevosteau@epicteam.com", phone="0033164753974")
        self.employee = Employee.objects.get(first_name="Louis")
        Employee.objects.filter(username="lprevosteau").first().groups.add(self.sales_departement)

        Client.objects.get_or_create(first_name='Alexandre', last_name='Normand',
                                     email='anormand@mymajorcompany.fr', phone='0033101020304',
                                     company_name='My Major Company', mobile='0033620103040')
        self.assigned_client = Client.objects.get(first_name='Alexandre')

        Client.objects.get_or_create(first_name='Guillaume', last_name='Normand',
                                     email='gnormand@mymajorcompany.fr', phone='0033101020304',
                                     company_name='My Major Company', mobile='0033620103040')
        self.not_assigned_client = Client.objects.get(first_name='Guillaume')

    def test_client_is_assigned(self):
        ClientAssignment.objects.get_or_create(client=self.assigned_client , employee=self.employee)
        self.assertEqual(self.assigned_client.is_assigned, True)

    def test_client_is_not_assigned(self):
        self.assertEqual(self.not_assigned_client.is_assigned, False)

    def test_client_is_prospect(self):
        self.assertEqual(self.assigned_client.is_prospect, True)

    def test_client_is_not_prospect(self):
        Contract.objects.get_or_create(client=self.assigned_client, amount_in_cts=20000)
        self.assertEqual(self.assigned_client.is_prospect, False)


class TestContract(TestCase):
    def setUp(self):
        Group.objects.get_or_create(id=2, name="Sales")
        self.sales_departement = Group.objects.get(id=2)
        Employee.objects.get_or_create(username="lprevosteau", first_name="Louis", last_name="Prevosteau",
                                       email="louis.prevosteau@epicteam.com", phone="0033164753974")
        Employee.objects.filter(username="lprevosteau").first().groups.add(self.sales_departement)

        self.employee = Employee.objects.get(first_name="Louis")

        Client.objects.get_or_create(first_name='Alexandre', last_name='Normand',
                                     email='anormand@mymajorcompany.fr', phone='0033101020304',
                                     company_name='My Major Company', mobile='0033620103040')
        self.client = Client.objects.get(first_name='Alexandre')

    def test_contract_has_registered_negotiator(self):
        Contract.objects.get_or_create(client=self.client, amount_in_cts=20000)
        contract = Contract.objects.get(client=self.client)
        ContractNegotiationAssignment.objects.get_or_create(contract=contract, employee=self.employee)
        self.assertEqual(contract.registered_negotiator, True)

    def test_contract_has_no_registered_negotiator(self):
        Contract.objects.get_or_create(client=self.client, amount_in_cts=20000)
        contract = Contract.objects.get(client=self.client)
        self.assertEqual(contract.registered_negotiator, False)

    def test_contract_is_signed(self):
        Contract.objects.get_or_create(client=self.client, amount_in_cts=20000)
        contract = Contract.objects.get(client=self.client)
        ContractSignatureAssignment.objects.get_or_create(contract=contract, employee=self.employee)
        self.assertEqual(contract.is_signed, True)

    def test_contract_is_not_signed(self):
        Contract.objects.get_or_create(client=self.client, amount_in_cts=20000)
        contract = Contract.objects.get(client=self.client)
        self.assertEqual(contract.is_signed, False)

    def test_contract_is_paid(self):
        Contract.objects.get_or_create(client=self.client, amount_in_cts=20000)
        contract = Contract.objects.get(client=self.client)
        ContractPaymentAssignment.objects.get_or_create(contract=contract, employee=self.employee)
        self.assertEqual(contract.is_paid, True)

    def test_contract_is_not_paid(self):
        Contract.objects.get_or_create(client=self.client, amount_in_cts=20000)
        contract = Contract.objects.get(client=self.client)
        self.assertEqual(contract.is_paid, False)

    def test_get_related_client_company_name(self):
        Contract.objects.get_or_create(client=self.client, amount_in_cts=20000)
        contract = Contract.objects.get(client=self.client)
        self.assertEqual(contract.client.company_name, 'My Major Company')

    def test_get_related_event_name(self):
        Contract.objects.get_or_create(client=self.client, amount_in_cts=20000)
        contract = Contract.objects.get(client=self.client)
        event_begin = datetime.datetime.now()
        event_end = event_begin + timedelta(days=1)
        Event.objects.get_or_create(contract=contract, name='Major Company Event', status=CREATED,
                                    begin_date=event_begin, end_date=event_end)
        self.assertEqual(contract.related_event_name, 'Major Company Event')

    def test_get_related_event_name_no_event(self):
        Contract.objects.get_or_create(client=self.client, amount_in_cts=20000)
        contract = Contract.objects.get(client=self.client)
        self.assertEqual(contract.related_event_name, '(No related event yet)')

    def test_get_contract_amount_in_euros(self):
        Contract.objects.get_or_create(client=self.client, amount_in_cts=20000)
        contract = Contract.objects.get(client=self.client)
        self.assertEqual(contract.amount_in_euros, 200)
