from django.test import TestCase
from django.contrib.auth.models import Group

from constants import MANAGER, SALES, SUPPORT
from core.models import Employee

# Create your tests here.


class TestEmployee(TestCase):
    def setUp(self):

        Group.objects.get_or_create(id=1, name="Management")
        Group.objects.get_or_create(id=2, name="Sales")
        Group.objects.get_or_create(id=3, name="Support")

        Employee.objects.get_or_create(username="mpotel", first_name="Martin", last_name="Potel",
                                       email="martin.potel@epicteam.com", phone="0033164752546")
        management = Group.objects.filter(name='Management').first()
        Employee.objects.filter(username="mpotel").first().groups.add(management)
        self.employee_martin = Employee.objects.get(first_name="Martin")

        Employee.objects.get_or_create(username="lprevosteau", first_name="Louis", last_name="Prevosteau",
                                       email="louis.prevosteau@epicteam.com", phone="0033164753974")
        sales = Group.objects.filter(name='Sales').first()
        Employee.objects.filter(username="lprevosteau").first().groups.add(sales)
        self.employee_louis = Employee.objects.get(first_name="Louis")

        Employee.objects.get_or_create(username="prognon", first_name="Pierre", last_name="Rognon",
                                       email="pierre.rognon@epicteam.com", phone="0033164757464")
        support = Group.objects.filter(name='Support').first()
        Employee.objects.filter(username="prognon").first().groups.add(support)
        self.employee_pierre = Employee.objects.get(first_name="Pierre")

        Employee.objects.get_or_create(first_name="Nicolas", last_name="Journet",
                                       email="nicolas.journet@epicteam.com", phone="0033164757561")
        self.employee_nicolas = Employee.objects.get(first_name="Nicolas")

    def test_employee_get_group(self):
        """
        Employee's department is returned
        """
        self.assertEqual(self.employee_martin.department.id, MANAGER)
        self.assertEqual(self.employee_louis.department.id, SALES)
        self.assertEqual(self.employee_pierre.department.id, SUPPORT)

    def test_employee_get_group_no_group(self):
        """
        Employee not affected to a department should return "(Not affected yet)"
        """
        self.assertEqual(self.employee_nicolas.department, 'Not affected yet')

    def test_employee_set_is_staff(self):
        """
        Sets employee to staff member, should return true
        """

        Employee.set_is_staff(self.employee_martin)
        self.employee_martin.refresh_from_db()
        self.assertEqual(self.employee_martin.is_staff, True)
